﻿'use strict';

module.exports = {
  load () {
    Editor.log('gif-to-animation load');
  },

  unload () {
    Editor.log('gif-to-animation unload');
  },

  messages: {
    'open-view' (event) {
      Editor.Panel.open('gif-to-animation.view');
    },
  },

};